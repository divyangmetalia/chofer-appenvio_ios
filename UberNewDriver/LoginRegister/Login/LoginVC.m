//
//  LoginVC.m
//  UberNewDriver
//
//  Created by Elluminati on 27/09/14.
//  Copyright (c) 2014 Elluminati. All rights reserved.
//

#import "LoginVC.h"
#import "UIImageView+Download.h"
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

static NSString *const kKeychainItemName = @"Google OAuth2 For gglplustest";
static NSString *const kClientID = GOOGLE_PLUS_CLIENT_ID;
static NSString *const kClientSecret = @"YVpSG_oz_YF4CHsherygbM1A";

@interface LoginVC ()
{
    AppDelegate *appDelegate;
    BOOL internet,isFacebookClicked;
    NSMutableArray *arrForCountry;
    NSMutableDictionary *dictparam;
    
    NSString * strEmail;
    NSString * strPassword;
    NSString * strLogin;
    NSMutableString * strSocialId;
    int reTrive;
}

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - View Life Cycle

@synthesize txtPassword,txtEmail;

- (void)viewDidLoad
{
    reTrive=0;
    [super viewDidLoad];
    isFacebookClicked=NO;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
   
    dictparam=[[NSMutableDictionary alloc]init];
    
    strEmail=[PREF objectForKey:PREF_EMAIL];
    strPassword=[PREF objectForKey:PREF_PASSWORD];
    strLogin=[PREF objectForKey:PREF_LOGIN_BY];
    strSocialId=[PREF objectForKey:PREF_SOCIAL_ID];
    
    internet=[APPDELEGATE connected];
    
    if(strEmail!=nil)
    {
        [self getSignIn];
    }
    
    [self customFont];
    
    [self.btnSignIn setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
    [self.btnForgotPsw setTitle:NSLocalizedString(@"FORGOT_PASSWORD", nil) forState:UIControlStateNormal];
    
    self.txtEmail.placeholder = NSLocalizedString(@"EMAIL", nil);
    self.txtPassword.placeholder = NSLocalizedString(@"PASSWORD", nil);
    self.lblSignInWih.text = NSLocalizedString(@"SIGN IN WITH", nil);
    self.lblUsername.text = NSLocalizedString(@"USERNAME", nil);
    self.lblPassword.text = NSLocalizedString(@"PASSWORD", nil);
    [self.btnDontHaveAccount setTitle:NSLocalizedString(@"don't account register", nil) forState:UIControlStateNormal];
    [self.btnDontHaveAccount setTitle:NSLocalizedString(@"don't account register", nil) forState:UIControlStateSelected];
    [self.btnBack setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
    [self.btnBack setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateSelected];

    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureLogin:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTapGestureRecognizer];
    
    //[self.scrLogin setContentSize:CGSizeMake(self.view.frame.size.width , self.btnForgotPsw.frame.size.height+self.btnForgotPsw.frame.origin.y)];
    //[self.scrLogin setContentSize:CGSizeMake(self.view.frame.size.width , self.view.frame.size.height)];
}

-(void)viewWillAppear:(BOOL)animated
{
    FBSDKLoginManager *logout = [[FBSDKLoginManager alloc] init];
    [logout logOut];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.btnSignUp setTitle:NSLocalizedString(@"SIGN UP", nil) forState:UIControlStateNormal];
}

-(void)handleSingleTapGestureLogin:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.txtEmail resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.scrLogin setContentOffset:offset animated:YES];
}

-(void)customFont
{
    UIColor *color = [UIColor darkGrayColor];
    
    self.txtEmail.font=[UberStyleGuide fontRegularLight];
    self.txtPassword.font=[UberStyleGuide fontRegularLight];
    self.lblOr.font=[UberStyleGuide fontRegularLight];
    self.lblSignInWih.font=[UberStyleGuide fontRegularLight];
    self.btnSignIn.titleLabel.font = [UberStyleGuide fontRegularLight];

    self.txtPassword.textColor = [UIColor darkGrayColor];
    self.txtEmail.textColor = [UIColor darkGrayColor];
    self.lblUsername.textColor = [UIColor darkGrayColor];
    self.lblOr.textColor = [UIColor darkGrayColor];
    self.lblSignInWih.textColor = [UIColor darkGrayColor];
    
    [self.btnForgotPsw setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnSignIn setTitleColor:[UberStyleGuide fontColor] forState:UIControlStateNormal];
    [self.btnSignUp setTitleColor:[UberStyleGuide fontColor] forState:UIControlStateNormal];
    
    UIFont *font = [UberStyleGuide fontRegularLight];
    [self.txtEmail setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtEmail setValue:font forKeyPath:@"_placeholderLabel.font"];
    [self.txtPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtPassword setValue:font forKeyPath:@"_placeholderLabel.font"];
    
    self.btnForgotPsw.titleLabel.font = [UberStyleGuide fontRegularBold];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Sign In

-(void)getSignIn
{
    if (device_token==nil || [device_token isEqualToString:@""] || [device_token isKindOfClass:[NSNull class]] || device_token.length < 1)
    {
        device_token=@"11111";
    }
    if (strEmail==nil)
    {
        strEmail=[PREF objectForKey:PREF_EMAIL];
        strPassword=[PREF objectForKey:PREF_PASSWORD];
        strLogin=[PREF objectForKey:PREF_LOGIN_BY];
        strSocialId=[PREF objectForKey:PREF_SOCIAL_ID];
    }
    if([APPDELEGATE connected] && self.txtEmail.text.length > 1)
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        
        [dictparam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
        [dictparam setObject:@"ios" forKey:PARAM_DEVICE_TYPE];
        [dictparam setObject:self.txtEmail.text forKey:PARAM_EMAIL];
        
        [dictparam setObject:strLogin forKey:PARAM_LOGIN_BY];
        if (![strLogin isEqualToString:@"manual"])
        {
            [dictparam setObject:strSocialId forKey:PARAM_SOCIAL_ID];
            
        }
        else
        {
            [dictparam setObject:strPassword forKey:PARAM_PASSWORD];
        }
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_LOGIN withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     arrUser=response;
                     
                     [PREF setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
                     [PREF setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
                     [PREF setObject:[NSString stringWithFormat:@"%@",device_token] forKey:PREF_DEVICE_TOKEN];
                     
                     [PREF setObject:txtEmail.text forKey:PREF_EMAIL];
                     [PREF setObject:txtPassword.text forKey:PREF_PASSWORD];
                     if(isFacebookClicked == NO)
                         [PREF setObject:@"manual" forKey:PREF_LOGIN_BY];
                     else
                         [PREF setObject:@"facebook" forKey:PREF_LOGIN_BY];
                     [PREF setBool:YES forKey:PREF_IS_LOGIN];
                     
                     [PREF setObject:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
                     
                     [PREF synchronize];
                     
                     txtPassword.userInteractionEnabled=YES;
                     [APPDELEGATE hideLoadingView];
                     [APPDELEGATE showToastMessage:(NSLocalizedString(@"SIGING_SUCCESS", nil))];
                     isFacebookClicked = NO;
                     [self performSegueWithIdentifier:@"seguetopickme" sender:self];
                     //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"REGISTER_SUCCESS", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     //            [alert show];
                 }
                 else
                 {
                     if([[response valueForKey:@"error"] intValue]==12)
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Driver is not a registered." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                     else
                     {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                     }
                 }
             }
             [APPDELEGATE hideLoadingView];
             NSLog(@"REGISTER RESPONSE --> %@",response);
         }];
    }
    else
    {
        if(self.txtEmail.text.length < 1)
        {
            if([strLogin isEqualToString:@"facebook"])
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"We are unable to fetch Email ID, Please Enter Email ID" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:NSLocalizedString(@"PLEASE_EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

#pragma mark -
#pragma mark - Button Action

- (IBAction)onClickSignIn:(id)sender
{
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    
    strEmail=self.txtEmail.text;
    strPassword=self.txtPassword.text;
    if(isFacebookClicked == YES)
        strLogin=@"facebook";
    else
        strLogin=@"manual";
    
    /* NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
     [pref setObject:txtEmail.text forKey:PREF_EMAIL];
     [pref setObject:txtPassword.text forKey:PREF_PASSWORD];
     [pref setObject:@"manual" forKey:PREF_LOGIN_BY];
     [pref setBool:YES forKey:PREF_IS_LOGIN];
     [pref synchronize];
     */
    [self getSignIn];
}

- (IBAction)googleBtnPressed:(id)sender
{
    /*[APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
    if([APPDELEGATE connected])
    {
        if ([[GooglePlusUtility sharedObject]isLogin])
        {
            [APPDELEGATE hideLoadingView];
            
        }
        else
        {
            [[GooglePlusUtility sharedObject]loginWithBlock:^(id response, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];
                 if (response) {
                     NSLog(@"Response ->%@ ",response);
                     txtPassword.userInteractionEnabled=NO;
                     self.txtEmail.text=[response valueForKey:@"email"];
                     
                     
                     [PREF setObject:txtEmail.text forKey:PREF_EMAIL];
                     [PREF setObject:@"google" forKey:PREF_LOGIN_BY];
                     [PREF setObject:[response valueForKey:@"userid"] forKey:PREF_SOCIAL_ID];
                     [PREF setBool:YES forKey:PREF_IS_LOGIN];
                     [PREF synchronize];
                     
                     
                     [self getSignIn];
                 }
             }];
        }
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }*/
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"google"];
    
    NSString *scope = kGTLAuthScopePlusLogin;
    GTMOAuth2Authentication * auth = [GTMOAuth2ViewControllerTouch
                                      authForGoogleFromKeychainForName:kKeychainItemName
                                      clientID:kClientID
                                      clientSecret:kClientSecret];
    GTMOAuth2ViewControllerTouch *authController;
    authController = [[GTMOAuth2ViewControllerTouch alloc]
                      initWithScope:scope
                      clientID:kClientID
                      clientSecret:kClientSecret
                      keychainItemName:kKeychainItemName
                      delegate:self
                      finishedSelector:@selector(viewController:finishedWithAuth:error:)];
    [[self navigationController] pushViewController:authController animated:YES];
    [auth beginTokenFetchWithDelegate:self didFinishSelector:@selector(auth:finishedRefreshWithFetcher:error:)];
}

- (void)auth:(GTMOAuth2Authentication *)auth finishedRefreshWithFetcher:(GTMHTTPFetcher *)fetcher error:(NSError *)error
{
    [self viewController:nil finishedWithAuth:auth error:error];
    if (error != nil)
    {
        NSLog(@"self .auth :%@",self.auth);
        
        NSLog(@"Authentication Error %@", error.localizedDescription);
        
        self.auth=nil;
        return;
    }
    self.auth=auth;
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
{
    if (error != nil)
    {
        //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Could not login" message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        //[alert show];
        NSLog(@"Authentication Error %@", error.localizedDescription);
        self.auth=nil;
        self.txtEmail.text = @"";
        self.txtPassword.text = @"";
        self.txtPassword.userInteractionEnabled = YES;
        [[GPPSignIn sharedInstance] signOut];
        [[GPPSignIn sharedInstance] disconnect];
        return;
    }
    else
    {
        [APPDELEGATE showLoadingWithTitle:@"Wait..."];
        self.auth=auth;
        auth.shouldAuthorizeAllRequests = YES;
        NSLog(@"login in");
        [self ForRetrive];
    }
}

-(void)ForRetrive
{
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    
    [plusService setAuthorizer:self.auth];
    
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error)
     {
         if (error)
         {
             GTMLoggerError(@"Error: %@", error);
         }
         else
         {
             reTrive++;
             [APPDELEGATE hideLoadingView];
             NSString *description = [NSString stringWithFormat:
                                      @"%@\n%@\n%@\n%@ Birthdate :%@ %@ %@", person.displayName,
                                      person.aboutMe,person.emails,person.birthday,person.image,person.name,person.gender];
             
             NSLog(@"response :%@",description);
             NSDictionary *dict=person.JSON;
             NSLog(@"Dict :%@",[dict valueForKey:@"emails"]);
             self.txtPassword.userInteractionEnabled = NO;
             NSMutableArray *arr=[[NSMutableArray alloc]init];
             arr=[dict valueForKey:@"emails"];
             NSDictionary *dictMain=[arr objectAtIndex:0];
             NSLog(@"array  :%@",[dictMain valueForKey:@"value"]);
             self.txtEmail.text = [dictMain valueForKey:@"value"];
             [PREF setValue:txtEmail.text forKey:PREF_EMAIL];
             [PREF setValue:@"google" forKey:PREF_LOGIN_BY];
             [PREF setValue:[dict valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
             [PREF setBool:YES forKey:PREF_IS_LOGIN];
             [PREF synchronize];

             NSLog(@"log for self auth :%@",self.auth);
             NSLog(@"new image :%@",person.image.url);
             if(reTrive==2)
             {
                 [appDelegate showLoadingWithTitle:NSLocalizedString(@"ALREADY_LOGIN", nil)];
                 [self getSignIn];
             }
         }
     }];
}

- (IBAction)facebookBtnPressed:(id)sender
{
    /*[[FacebookUtility sharedObject] logOutFromFacebook];
    
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
    if([APPDELEGATE connected])
    {
        if (![[FacebookUtility sharedObject]isLogin])
        {
            [[FacebookUtility sharedObject]loginInFacebook:^(BOOL success, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];
                 if (success)
                 {
                     NSLog(@"Success");
                     appDelegate = [UIApplication sharedApplication].delegate;
                     [appDelegate userLoggedIn];
                     [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error)
                      {
                          if (response)
                          {
                              isFacebookClicked=YES;
                              NSLog(@"%@",response);
                              self.txtEmail.text=[response valueForKey:@"email"];
                              txtPassword.userInteractionEnabled=NO;
                              txtPassword.text=@"";
                              [PREF setObject:[response valueForKey:@"email"] forKey:PREF_EMAIL];
                              [PREF setObject:@"facebook" forKey:PREF_LOGIN_BY];
                              [PREF setObject:[response valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
                              [PREF setBool:YES forKey:PREF_IS_LOGIN];
                              [PREF synchronize];
                              [self getSignIn];
                          }
                      }];
                 }
             }];
        }
        else
        {
            NSLog(@"User Login Click");
            appDelegate = [UIApplication sharedApplication].delegate;
            [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error) {
                [APPDELEGATE hideLoadingView];
                
                if (response) {
                    NSLog(@"%@",response);
                    NSLog(@"%@",response);
                    self.txtEmail.text=[response valueForKey:@"email"];
                    txtPassword.userInteractionEnabled=NO;
                    
                    [PREF setObject:[response valueForKey:@"email"] forKey:PREF_EMAIL];
                    [PREF setObject:@"facebook" forKey:PREF_LOGIN_BY];
                    [PREF setObject:[response valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
                    [PREF setBool:YES forKey:PREF_IS_LOGIN];
                    [PREF synchronize];
                    [self getSignIn];
                    
                }
            }];
            
            [appDelegate userLoggedIn];
            
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }*/
    
    if ([APPDELEGATE connected])
    {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager
         logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             if (error) {
                 NSLog(@"Process error");
             } else if (result.isCancelled){
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in");
                 [APPDELEGATE showLoadingWithTitle:@"Please wait"];
                 
                 if ([FBSDKAccessToken currentAccessToken])
                 {
                     FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                   initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
                                                   HTTPMethod:@"GET"];
                     [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                           id result,
                                                           NSError *error)
                      {
                          // Handle the result
                          [APPDELEGATE hideLoadingView];
                          isFacebookClicked=YES;
                          self.txtPassword.userInteractionEnabled=NO;
                          NSLog(@"FB Response ->%@",result);
                          self.txtEmail.text=[result valueForKey:@"email"];
                          [PREF setObject:[result valueForKey:@"email"] forKey:PREF_EMAIL];
                          [PREF setObject:@"facebook" forKey:PREF_LOGIN_BY];
                          [PREF setObject:[result valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
                          [PREF setBool:YES forKey:PREF_IS_LOGIN];
                          [PREF synchronize];
                          [self getSignIn];
                      }];
                 }
             }
         }];
        
        [loginManager logOut];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

- (IBAction)forgotBtnPressed:(id)sender
{
    
}

- (IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int y=60;
    if (textField==self.txtEmail)
    {
        y=60;
    }
    else if (textField==self.txtPassword){
        y=120;
    }
    [self.scrLogin setContentOffset:CGPointMake(0, y) animated:YES];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.txtEmail)
    {
        if(txtPassword.userInteractionEnabled==NO)
        {
            [textField resignFirstResponder];
            [self.scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        else
            [self.txtPassword becomeFirstResponder];
    }
    else if (textField==self.txtPassword){
        [textField resignFirstResponder];
        [self.scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    return YES;
}

/*
 #pragma mark-
 #pragma mark- Text Field Delegate
 
 - (BOOL)textFieldShouldReturn:(UITextField *)textField     //Hide the keypad when we pressed return
 {
 if (textField==txtEmail)
 {
 [self.txtPassword becomeFirstResponder];
 }
 
 [textField resignFirstResponder];
 return YES;
 }
 
 - (void)textFieldDidBeginEditing:(UITextField *)textField
 
 {
 if(textField == self.txtPassword)
 {
 UITextPosition *beginning = [self.txtPassword beginningOfDocument];
 [self.txtPassword setSelectedTextRange:[self.txtPassword textRangeFromPosition:beginning
 toPosition:beginning]];
 [UIView animateWithDuration:0.3 animations:^{
 
 self.view.frame = CGRectMake(0, -35, 320, 480);
 
 } completion:^(BOOL finished) { }];
 }
 }
 - (void)textFieldDidEndEditing:(UITextField *)textField
 {
 UIDevice *thisDevice=[UIDevice currentDevice];
 if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
 {
 CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
 
 if (iOSDeviceScreenSize.height == 568)
 {
 if(textField == self.txtPassword)
 {
 [UIView animateWithDuration:0.3 animations:^{
 
 self.view.frame = CGRectMake(0, 0, 320, 568);
 
 } completion:^(BOOL finished) { }];
 }
 
 }
 else
 {
 
 if(textField == self.txtPassword)
 {
 [UIView animateWithDuration:0.3 animations:^{
 
 self.view.frame = CGRectMake(0, 0, 320, 480);
 
 } completion:^(BOOL finished) { }];
 }
 
 
 }
 }
 }
 */


@end
